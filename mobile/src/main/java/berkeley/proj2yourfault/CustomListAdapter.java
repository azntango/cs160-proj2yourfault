package berkeley.proj2yourfault;

/**
 * Created by Allan on 10/13/2015.
 */
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> itemname;
    private final ArrayList<String> itemname2;

    public CustomListAdapter(Activity context, ArrayList<String> itemname, ArrayList<String> itemname2, ArrayList<String> both) {
        super(context, R.layout.mylist, both);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.itemname=itemname;
        this.itemname2=itemname2;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.mylist, null,true);

        if (position % 2 == 1) {
            rowView.setBackgroundColor(0xFFE3D5B8);
        } else {
            rowView.setBackgroundColor(0xFF95D1C5);
        }

        TextView txtTitle = (TextView) rowView.findViewById(R.id.item);
        TextView extratxt = (TextView) rowView.findViewById(R.id.textView1);

        txtTitle.setText(itemname.get(position));
        extratxt.setText(itemname2.get(position) + 'M');
        return rowView;

    };
}