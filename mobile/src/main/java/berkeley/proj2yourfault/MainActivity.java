package berkeley.proj2yourfault;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.NotificationCompat;
import android.app.Notification;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    // Variables pertaining to Earthquake data and UI
    ListView listView ;
    ArrayAdapter<String> adapter;
    BroadcastReceiver receiver;
    ArrayList <String> location_list;
    ArrayList <String> magnitude_list;
    ArrayList <String> both;
    int notificationID = 1;
    public List<List<Object>> eqData = new ArrayList<>();

    // Location Listener Variables
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    double currentLatitude;
    double currentLongtitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Google API
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Location Listener
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        // Start querying the Earthquake data
        Intent i = new Intent(this, EarthquakeService.class);
        startService(i);

        /**
         *  Initialize the ListView
         */

        listView = (ListView) findViewById(R.id.list);
        location_list = new ArrayList<String>();
        Collections.addAll(location_list, "Loading...");
        magnitude_list = new ArrayList<String>();
        Collections.addAll(magnitude_list, "");
        both = new ArrayList<String>();
        Collections.addAll(magnitude_list, "");

        adapter = new CustomListAdapter(this, location_list, magnitude_list, both);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String[] information = ((String) listView.getItemAtPosition(position)).split(":");
                String place = information[0];
                String magnitude = information[1];
                String time = information[2];
                String longtitude = information[3];
                String latitude = information[4];
                String degree = information[5];

                // Get current location and calculate distance away
                Location current = new Location("");
                current.setLatitude(currentLatitude);
                current.setLongitude(currentLongtitude);

                Location eqloc = new Location("");
                eqloc.setLatitude(Double.parseDouble(latitude));
                eqloc.setLongitude(Double.parseDouble(longtitude));

                float distanceInMeters = current.distanceTo(eqloc);

                //Prepare intent for notification to watch
                Intent imageIntent = new Intent(view.getContext(), Image.class);
                imageIntent.putExtra("coordinates", latitude + ":" + longtitude);
                PendingIntent pIntent = PendingIntent.getActivity(view.getContext(), (int) System.currentTimeMillis(), imageIntent, 0);

                Notification notification = new NotificationCompat.Builder(getApplication())
                        .setSmallIcon(R.drawable.eq)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000, 1000})
                        .addAction(R.drawable.eq, "More", pIntent)
                        .setContentTitle(place)
                        .setContentText(magnitude + "M\n" + Float.toString(distanceInMeters/1000) + " km")
                        .extend(new NotificationCompat.WearableExtender().setHintShowBackgroundOnly(true))
                        .build();

                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplication());
                notificationManager.notify(++notificationID, notification);

                // Show Google Maps Activity
                Intent i = new Intent(view.getContext(), GPSActivity.class);
                Log.d(TAG, "Making intent");
                i.putExtra("coordinates", latitude + ":" + longtitude +
                ":" + place + ":" + magnitude + "M" + ":" + Float.toString(distanceInMeters/1000) + " km");
                startActivity(i);
            }
        });

        /**
         *  Create Listener for updates in EQdata. Parse the JSON. Update ListView
         */

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Remove old data
                Log.d(TAG, "NEW DATA!");
                eqData = new ArrayList<>();
                location_list.clear();
                magnitude_list.clear();

                String s = intent.getStringExtra(EarthquakeService.COPA_MESSAGE);
                JSONObject jObj = null;
                try {
                    jObj = new JSONObject(s);
                    JSONArray feat = jObj.getJSONArray("features");
                    for (int i = 0; i < 19; i++) {
                        JSONObject earthQuake = feat.getJSONObject(i);
                        JSONObject properties = earthQuake.getJSONObject("properties");

                        //Property elements
                        String[] place_temp = properties.getString("place").split("of ");
                        String place = place_temp[1];
                        Double magnitude = properties.getDouble("mag");
                        Double time = properties.getDouble("time");

                        //Geometry elements
                        JSONArray coordinates = earthQuake.getJSONObject("geometry").getJSONArray("coordinates");
                        Object latitude = coordinates.get(0);
                        Object longtitude = coordinates.get(1);
                        Object degree = coordinates.get(2);

                        // ID element
                        String id = earthQuake.getString("id");

                        //Create array to store elements
                        List<Object> elements = new ArrayList();
                        elements.add(0, id);
                        elements.add(1, place);
                        elements.add(2, magnitude);
                        elements.add(3, time);
                        elements.add(4, latitude);
                        elements.add(5, longtitude);
                        elements.add(6, degree);
                        //Insert into Arraylist
                        eqData.add(elements);

                        //Insert only the place and magnitude into the UI array
                        location_list.add(place);
                        magnitude_list.add(magnitude.toString());
                        both.add(place + ":" + magnitude.toString() + ":" + time.toString() + ":"
                                + latitude.toString() + ":" + longtitude.toString() + ":" + degree.toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Update UI ListView
                adapter.notifyDataSetChanged();
            }
        };

    }
    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(EarthquakeService.COPA_RESULT)
        );
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            Log.d(TAG, "Location is null!");
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        else {
            handleNewLocation(location);
        }
    }

    private void handleNewLocation(Location location) {
        currentLatitude = location.getLatitude();
        currentLongtitude = location.getLongitude();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }
}
