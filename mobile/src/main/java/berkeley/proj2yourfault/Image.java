package berkeley.proj2yourfault;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class Image extends Activity {

    private static final String TAG = Image.class.getSimpleName();
    BroadcastReceiver receiver;

    ImageView iv;
    Bitmap image ;
    String image_link;

    @Override
    protected void onCreate(Bundle savedInstanceStance) {
        super.onCreate(savedInstanceStance);
        setContentView(R.layout.image);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String coordinates = bundle.getString("coordinates");
        String eqlatitude = coordinates.split(":")[0];
        String eqlongtitude = coordinates.split(":")[1];

        Log.d(TAG, "Sending request to service");
        Intent i = new Intent(this, ImageService.class);
        i.putExtra("coordinates", eqlatitude + ":" + eqlongtitude);
        startService(i);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Log.d(TAG, "Received something!");
                Random random = new Random();

                String s = intent.getStringExtra(ImageService.COPA_MESSAGE2);
                Log.d(TAG, s);
                JSONObject jObj = null;
                if (s != null){
                    try {
                        jObj = new JSONObject(s);
                        JSONArray images = jObj.getJSONArray("data");
                        JSONObject first_image_data = images.getJSONObject(random.nextInt(images.length() + 1));
                        JSONObject image = first_image_data.getJSONObject("images");
                        System.out.println(image.toString());
                        image_link = image.getJSONObject("standard_resolution").getString("url");
                        Log.d(TAG, image_link);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    iv = (ImageView) findViewById(R.id.imageView1);
                    new TheTask().execute();
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(ImageService.COPA_RESULT2)
        );
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    class TheTask extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try
            {
                image = downloadBitmap(image_link);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            Log.d(TAG, "PostExecute");
            if(image!=null)
            {
                Log.d(TAG, "Showing image");
                iv.setImageBitmap(image);
                iv.setScaleType(ImageView.ScaleType.FIT_XY);
            }

        }
    }
    private Bitmap downloadBitmap(String url1) {
        InputStream is = null;
        try {
            URL url = new URL(url1);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int res = conn.getResponseCode();
            Log.d(TAG, "The image response is: " + res);
            is = conn.getInputStream();

            image = BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return image;
    }
}

