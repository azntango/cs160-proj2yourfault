package berkeley.proj2yourfault;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Allan on 10/12/2015.
 */
public class EarthquakeService extends Service {

    private static final int INTERVAL = 10000;
    private static final int SECOND = 1000;

    private static final String TAG = MainActivity.class.getSimpleName();

    private GoogleApiClient mApiClient;
    private String urlString = "http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson";

    //Broadcast metadata variables
    static final public String COPA_RESULT = "com.controlj.copame.backend.COPAService.REQUEST_PROCESSED";
    static final public String COPA_MESSAGE = "com.controlj.copame.backend.COPAService.COPA_MSG";
    LocalBroadcastManager broadcaster;

    String eqResponse = "";
    String neweqResponse = "";

    @Override
    public void onCreate() {
        super.onCreate();

        //Create a broadcaster to send out message to update Earthquake list
        broadcaster = LocalBroadcastManager.getInstance(this);
        
        //initialize the googleAPIClient for message passing
        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                    }
                })
                .build();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Kick off new work to do
        getEQData(0);
        return START_STICKY;
    }


    private void getEQData(int interval) {

        CountDownTimer timer = new CountDownTimer(interval, SECOND) {
            public void onTick(long millisUntilFinished) {}

            public void onFinish() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        /**
                         * First establish HTTP connection and get data as a long string
                         */

                        InputStream is = null;
                        try {
                            URL url = new URL(urlString);
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setReadTimeout(10000 /* milliseconds */);
                            conn.setConnectTimeout(15000 /* milliseconds */);
                            conn.setRequestMethod("GET");
                            conn.setDoInput(true);
                            conn.connect();
                            int res = conn.getResponseCode();
                            Log.d(TAG, "The response is: " + res);
                            is = conn.getInputStream();

                            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                            StringBuilder response = new StringBuilder(); // or StringBuffer if not Java 5+
                            String line;
                            while ((line = rd.readLine()) != null) {
                                response.append(line);
                                response.append('\r');
                            }
                            rd.close();
                            neweqResponse = response.toString();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (is != null) {
                                try {
                                    is.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        //if data has changed, update the ListView
                        if (!eqResponse.equals(neweqResponse)) {
                            eqResponse = neweqResponse;
                            /**
                             *  Update ListView here!
                             */
                            sendEQData(eqResponse);
                        }

                    }
                }).start();

                // Poll the data again
                getEQData(INTERVAL);
            }
        };

        timer.start();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


    //Method to send EQdata to Main thread to update UI
    public void sendEQData(String message) {
        Intent intent = new Intent(COPA_RESULT);
        if(message != null)
            intent.putExtra(COPA_MESSAGE, message);
        broadcaster.sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mApiClient.disconnect();
    }
}
