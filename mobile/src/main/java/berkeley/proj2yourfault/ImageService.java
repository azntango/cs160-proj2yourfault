package berkeley.proj2yourfault;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageService extends Service {

    private static final String TAG = ImageService.class.getSimpleName();

    //Broadcast metadata variables
    static final public String COPA_RESULT2 = "com.controlj.copame.backend.COPAService.REQUEST_PROCESSED2";
    static final public String COPA_MESSAGE2 = "com.controlj.copame.backend.COPAService.COPA_MSG2";
    LocalBroadcastManager broadcaster;

    private GoogleApiClient mApiClient;

    String urlString;
    String eqlatitude;
    String eqlongitude;
    String imageResponse;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "I'm in the service now");

        //Create a broadcaster to send out message to update Earthquake list
        broadcaster = LocalBroadcastManager.getInstance(this);

        //initialize the googleAPIClient for message passing
        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                    }
                })
                .build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Kick off new work to do

        Log.d(TAG, "Starting service");

        Bundle bundle = intent.getExtras();
        String coordinates = bundle.getString("coordinates");
        eqlatitude = coordinates.split(":")[0];
        eqlongitude = coordinates.split(":")[1];
        urlString = "https://api.instagram.com/v1/media/search?lat=" +
                eqlatitude + "&lng=" + eqlongitude + "&access_token=2238492190.d1dbad1.38083fd0d971434a8b00992ec6b9e7a3";
        getImage();
        return START_STICKY;
    }

    private void getImage() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                InputStream is = null;
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000 /* milliseconds */);
                    conn.setConnectTimeout(15000 /* milliseconds */);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    int res = conn.getResponseCode();
                    is = conn.getInputStream();

                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    StringBuilder response = new StringBuilder(); // or StringBuffer if not Java 5+
                    String line;
                    while ((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                    imageResponse = response.toString();
                    Log.d(TAG, imageResponse);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                sendEQData(imageResponse);
            }

        }).start();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void sendEQData(String message) {
        Intent intent = new Intent(COPA_RESULT2);
        if(message != null)
            Log.d(TAG, "This is message being sent");
            Log.d(TAG, message);
        intent.putExtra(COPA_MESSAGE2, message);
        broadcaster.sendBroadcast(intent);
    }
}
